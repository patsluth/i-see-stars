//
//  SWISSRatingControl.h
//  SWISeeStars
//
//  Created by Pat Sluth on 2014-06-29.
//
//

#import <UIKit/UIKit.h>

@interface SWISSRatingControl : UIView

@property (nonatomic) NSInteger rating;

- (void)updateRenderingMode:(UIImageRenderingMode)renderingMode;

@end




